package com.sinam.repository;

import com.sinam.model.Case;
import org.springframework.data.repository.CrudRepository;

public interface CaseRepository extends CrudRepository<Case,Long>{

}
