package com.sinam.repository;

import com.sinam.model.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends CrudRepository<Document,Long> {
    @Query(value = "select id,name,type from document", nativeQuery = true)
    public List<?> findCustom();

}
