package com.sinam.web;


import com.sinam.services.SearchingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class SearchContent {


    @Value("${app.index}")
    private String index;
    @RequestMapping(value = "/search/{content}",method = RequestMethod.GET)
    public ArrayList<String> search(@PathVariable("content") String content){

        SearchingService searchingService =new SearchingService();
        ArrayList<String> s= searchingService.search(content,index);
        return s;
    }
}
