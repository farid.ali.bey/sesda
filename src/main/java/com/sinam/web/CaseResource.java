package com.sinam.web;


import com.sinam.model.Case;
import com.sinam.services.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/case")
public class CaseResource {

    @Autowired
    private CaseService caseService;

    @RequestMapping(value="/create", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Case> doCreate( @Valid @RequestBody Case ccc){

            caseService.save(ccc);
            return ResponseEntity.ok().body(ccc);

    }
    @GetMapping("/{id}")
    public String select(@PathVariable("id") Long id){
        return caseService.get(id).toString();
    }
}
