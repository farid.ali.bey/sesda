package com.sinam.web;


import com.sinam.model.Log;
import com.sinam.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/log")
public class LogResource {
    @Autowired
    LogService logService;

    @RequestMapping(value = "/all",
            method=RequestMethod.GET,
            consumes="application/json",
            produces="application/json")
    public ResponseEntity<List<Log>> getAll(Principal principal){

        return new ResponseEntity<List<Log>>(logService.findAll(), HttpStatus.OK);
        }
}
