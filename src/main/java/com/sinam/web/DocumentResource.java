package com.sinam.web;

import com.sinam.model.Document;
import com.sinam.services.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DocumentResource {
    @Autowired
    private  DocumentService documentService;




    @PostMapping(value = "/add",consumes = { "multipart/form-data", MediaType.APPLICATION_JSON_VALUE})
    public void doAdd(@RequestParam("file") MultipartFile file, Principal principal){


        documentService.addDocument(file,principal.getName());
    }


    @GetMapping("/document/{id}")
    public ResponseEntity<InputStreamResource> doGet(@PathVariable("id") Long id) throws IOException {
        Document document=documentService.getDocument(id);
        InputStreamResource inputStream=new InputStreamResource(new FileInputStream(document.getView()));
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(inputStream);


    }

    @GetMapping(value = "/list",
            consumes="application/json",
            produces="application/json")
    public ResponseEntity<List<Document>> list(){

       return new ResponseEntity<List<Document>>((List<Document>) documentService.findAll(), HttpStatus.OK);

    }


}
