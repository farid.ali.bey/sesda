package com.sinam.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@RestController
public class Welcome {
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String login(Principal principal,HttpSession session){
        System.out.println("hello");
        return "{ 'username':'"+ principal.getName()+"'}";
    }
    @RequestMapping(value = "/",method = RequestMethod.POST)
    public String loginpost(Principal principal, HttpSession session){

        return principal.getName();
    }
}
