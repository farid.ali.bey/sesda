package com.sinam.security.rest;

import com.sinam.security.model.User;
import com.sinam.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserResource {
    @Autowired
    private UserService userService;
    @PostMapping("/create")
    public User create(@RequestBody User user){


        return userService.save(user);
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id){

        userService.delete(id);
        return "deleted";
    }
    @GetMapping("/select/{id}")
    public User select(@PathVariable("id") Long id){
        return userService.select(id);
    }

}
