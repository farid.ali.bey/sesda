package com.sinam.security.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String org_name;
    private String org_short_name;
    private String address;
    private String phone;
    private String fax;

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_short_name() {
        return org_short_name;
    }

    public void setOrg_short_name(String org_short_name) {
        this.org_short_name = org_short_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Organization(String org_name, String org_short_name, String address, String phone, String fax) {
        this.org_name = org_name;
        this.org_short_name = org_short_name;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "id=" + id +
                ", org_name='" + org_name + '\'' +
                ", org_short_name='" + org_short_name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                '}';
    }
}
