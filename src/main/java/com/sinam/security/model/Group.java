package com.sinam.security.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(unique=true)
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<User> users;

    private String groupForUser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupForUser() {
        return groupForUser;
    }

    public void setGroupForUser(String groupForUser) {
        this.groupForUser = groupForUser;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Group && obj.getClass()==Group.class){
            Group group=(Group) obj;
            return group.getName().equalsIgnoreCase(this.name);
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", users=" + users +
                ", groupForUser='" + groupForUser + '\'' +
                '}';
    }
}