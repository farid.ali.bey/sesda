package com.sinam.services;


import com.sinam.model.Case;
import com.sinam.repository.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CaseService {
    @Autowired
    private CaseRepository caseRepository;

    public Case save(Case c){

        return caseRepository.save(c);
    }
    public Case get(Long id){
        return caseRepository.findById(id).get();
    }
}
