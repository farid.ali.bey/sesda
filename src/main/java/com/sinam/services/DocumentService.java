package com.sinam.services;


import com.aspose.words.SaveFormat;
import com.aspose.words.SaveOutputParameters;
import com.sinam.model.Document;
import com.sinam.repository.DocumentRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DocumentService {
   @Autowired
    private DocumentRepository documentRepository;

    @Value("${app.storage}")
    private String storage;

    @Value("${app.index}")
    private  String index;

   public Document addDocument(MultipartFile file, String user){
        Document document=new Document();
        document.setName(file.getOriginalFilename());
        document.setType(file.getContentType());

       Date date=new Date();
       SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY/MM/dd");


       File fileDir=new File(storage+simpleDateFormat.format(date));
       if(!fileDir.exists())
           fileDir.mkdirs();

       File f=new File(storage+simpleDateFormat.format(date)+"/"+this.generateFileName()+"."+ FilenameUtils.getExtension(file.getOriginalFilename()));

       try {
           file.transferTo(f);

       } catch (IOException e) {
           e.printStackTrace();
       }
        document.setPath(f.getPath());
       if (FilenameUtils.getExtension(f.getName()).equals("docx")){

           com.aspose.words.Document docx2PDF= null;
           SaveOutputParameters pdf=null;
           try {
               docx2PDF = new com.aspose.words.Document(f.getPath());
               pdf=docx2PDF.save(f.getPath().replace("docx","pdf"), SaveFormat.PDF);
           } catch (Exception e) {
               e.printStackTrace();
           }

           document.setView(f.getPath().replace("docx","pdf"));
       }else {

           document.setView(f.getPath());
       }
          document.setOwner(user);
       documentRepository.save(document);
       IndexingService indexingService =new IndexingService();
       indexingService.indexFile(document.getView(),index);
       return document;
   }
   public Document getDocument(Long id){



       return documentRepository.findById(id).get();

   }

   public Iterable<Document> findAll(){

       return documentRepository.findAll();
   }

   public List<?> findCustom(){

       return documentRepository.findCustom();
   }
    public String generateFileName() {
        Date date=new Date();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY/MM/dd");
        File f=new File(storage+simpleDateFormat.format(date));
        return Integer.toString(f.listFiles().length+1);

    }
}
