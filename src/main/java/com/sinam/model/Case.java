package com.sinam.model;


import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Case {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String case_id;
    private String case_type;
    private String case_subject;
    private String case_sub_subject;
    private String status;
    private String received_doc_id;
    private String controlled_by_sender;
    private String controlled_by_org;
    private String instruction;
    private String instruction_owner;
    private String address_to;
    private String page_count;
    private String copy_count;
    private String approved_by;
    @CreationTimestamp
    private Date   creation_date;
    private Date   due_date;
    private Date   instruction_date;
    private Date   approved_date;
    private Date   received_date;
    private Date   registration_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCase_id() {
        return case_id;
    }

    public void setCase_id(String case_id) {
        this.case_id = case_id;
    }

    public String getCase_type() {
        return case_type;
    }

    public void setCase_type(String case_type) {
        this.case_type = case_type;
    }

    public String getCase_subject() {
        return case_subject;
    }

    public void setCase_subject(String case_subject) {
        this.case_subject = case_subject;
    }

    public String getCase_sub_subject() {
        return case_sub_subject;
    }

    public void setCase_sub_subject(String case_sub_subject) {
        this.case_sub_subject = case_sub_subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceived_doc_id() {
        return received_doc_id;
    }

    public void setReceived_doc_id(String received_doc_id) {
        this.received_doc_id = received_doc_id;
    }

    public String getControlled_by_sender() {
        return controlled_by_sender;
    }

    public void setControlled_by_sender(String controlled_by_sender) {
        this.controlled_by_sender = controlled_by_sender;
    }

    public String getControlled_by_org() {
        return controlled_by_org;
    }

    public void setControlled_by_org(String controlled_by_org) {
        this.controlled_by_org = controlled_by_org;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getInstruction_owner() {
        return instruction_owner;
    }

    public void setInstruction_owner(String instruction_owner) {
        this.instruction_owner = instruction_owner;
    }

    public String getAddress_to() {
        return address_to;
    }

    public void setAddress_to(String address_to) {
        this.address_to = address_to;
    }

    public String getPage_count() {
        return page_count;
    }

    public void setPage_count(String page_count) {
        this.page_count = page_count;
    }

    public String getCopy_count() {
        return copy_count;
    }

    public void setCopy_count(String copy_count) {
        this.copy_count = copy_count;
    }

    public String getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(String approved_by) {
        this.approved_by = approved_by;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public Date getInstruction_date() {
        return instruction_date;
    }

    public void setInstruction_date(Date instruction_date) {
        this.instruction_date = instruction_date;
    }

    public Date getApproved_date() {
        return approved_date;
    }

    public void setApproved_date(Date approved_date) {
        this.approved_date = approved_date;
    }

    public Date getReceived_date() {
        return received_date;
    }

    public void setReceived_date(Date received_date) {
        this.received_date = received_date;
    }

    public Date getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }


}
