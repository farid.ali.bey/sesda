package com.sinam.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String sender;
    private String performer;
    private String status;
    private String sender_org;
    private String performer_org;
    private String sender_note;
    private String performer_note;
    private String action;
    private Date   sent_date;
    private Date   end_date;

    @ManyToOne
    @NotNull
    private Case aCase;


    public Log() {
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSender_org() {
        return sender_org;
    }

    public void setSender_org(String sender_org) {
        this.sender_org = sender_org;
    }

    public String getPerformer_org() {
        return performer_org;
    }

    public void setPerformer_org(String performer_org) {
        this.performer_org = performer_org;
    }

    public String getSender_note() {
        return sender_note;
    }

    public void setSender_note(String sender_note) {
        this.sender_note = sender_note;
    }

    public String getPerformer_note() {
        return performer_note;
    }

    public void setPerformer_note(String performer_note) {
        this.performer_note = performer_note;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getSent_date() {
        return sent_date;
    }

    public void setSent_date(Date sent_date) {
        this.sent_date = sent_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Case getaCase() {
        return aCase;
    }

    public void setaCase(Case aCase) {
        this.aCase = aCase;
    }
}
