FROM openjdk:8
ADD target/sesda.jar  sesda.jar
EXPOSE 8888
ENTRYPOINT ["java","-jar","sesda.jar"]